import cv2
import yaml

cfg_name = 'conf/camera.yaml'
with open(cfg_name, 'r') as file:
    cfg = yaml.safe_load(file)
url = cfg['capture_url']

cap = cv2.VideoCapture(url)

while True:
    ret, frame = cap.read()
    cv2.imshow("frame", frame)
    k = cv2.waitKey(1)
    if k == ord('q'):
        cv2.destroyAllWindows()
        exit(1)

    elif k == 32: # space
        cv2.imwrite('img.jpg', frame)


