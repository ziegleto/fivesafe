import cv2
import sys
import socket
import yaml
import pickle
from bev import PositionEstimation, draw_world_position
from image_tracking import Tracker
from decision_module import decision_making as dm
from utilities import connect_socket

def handle_turn_right():
    pass

def handle_leaving_path():
    pass

def handle_crowded():
    pass

def main(
    connection, 
    img_width, 
    img_height, 
    homography_fname, 
    contours_int_path, 
    color_int_path, 
    contours_turn_right, 
    color_turn_right, 
    top_view_fname, 
    color_standard
) -> None:
    top_view_org = cv2.imread(top_view_fname)  # TODO
    tv_height, tv_width, _ = top_view_org.shape
    tracker = Tracker(img_width, img_height)
    pos_est = PositionEstimation(homography_fname, 81/5) 

    while True:
        data = connection.recv(4096) #this can be an error, if size bigger. Truncated...
        detections = pickle.loads(data)
        tracks = tracker.track(detections)
        nr_of_objects = len(tracks)
        top_view = top_view_org.copy()

        # Calculate BEV & Draw Tracks
        for track in tracks:
            mask = detections[track.detection_id-1].mask
            world_position, _ = pos_est.map_entity_and_return_relevant_points(track, mask)
            world_position = (world_position[0], world_position[1])

            # Decision Making
            color = color_standard
            if dm.is_pt_in_contours(world_position, contours_int_path):
                color = color_int_path
                handle_leaving_path()
            if dm.is_pt_in_contours(world_position, contours_turn_right):
                color = color_turn_right
                handle_turn_right()
            if dm.is_crowded(nr_of_objects, 1):
                top_view = dm.draw_crowded(
                    top_view, 
                    tv_width, 
                    tv_height, 
                    thickness=40, 
                    font_thickness=5, 
                    font_scale=5, 
                    y_offset=200
                )
                handle_crowded()

            track.world_position = world_position
            track.color = color
            
        msg = pickle.dumps(tracks)
        sock.send(msg)

            # Draw Positions in Top-View
            #top_view = draw_world_position(top_view, world_position, track.id, color)
        #cv2.imshow("top_view", top_view)
        #cv2.waitKey(1)

if __name__ == '__main__':
    # 1. Load config 
    cfg_name = 'conf/network/edge.yaml'
    with open(cfg_name, 'r') as file:
            cfg = yaml.safe_load(file)

    url = cfg['edge_url']
    port = cfg['edge_port']
    url_out = cfg['url_out']
    port_out = cfg['port_out']
    homography_fname = cfg['homography_fname']
    img_width = cfg['img_width']
    img_height = cfg['img_height']
    cfg_intended_paths = cfg['dm_intended_paths']
    cfg_turn_right = cfg['dm_turn_right']
    top_view_fname = cfg['top_view_fname']
    color_standard = cfg['color_standard']

    contours_int_path, color_int_path = dm.init_contours(cfg_intended_paths)
    contours_turn_right, color_turn_right = dm.init_contours(cfg_turn_right)

    # 2. Connect 
    # Input
    connection, _ = connect_socket(url, port)
    # Output
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (url_out, port_out)
    print(server_address)
    sock.connect(server_address)

    # 3. Go
    main(
        connection, 
        img_width, 
        img_height, 
        homography_fname, 
        contours_int_path, 
        color_int_path, 
        contours_turn_right, 
        color_turn_right,
        top_view_fname,
        color_standard
    )