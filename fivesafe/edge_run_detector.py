import numpy as np
import cv2
from object_detection import Detectorv8Seg, draw_detections
from utilities import run
from ultralytics import YOLO
import socket
import pickle
import yaml


def start(cap, homography_fname, top_view_fname, model_name):
    model = YOLO('yolov8n-seg.pt')
    detector = Detectorv8Seg(model=model, classes_of_interest=['car', 'person', 'bicycle'])

    while True:
        ret, frame = cap.read()
        if not ret:
            print("Error: Failed to read fname from RTSP Stream")
            break
        detections = detector.detect(frame)
        msg = '\u007b"detections": [' 
        draw_detections(frame, detections, mask=True)
        # Track
        for detection in detections:
            hull = cv2.convexHull(
                detection.mask.reshape(-1, 1, 2), 
                cv2.RETR_EXTERNAL, 
                cv2.CHAIN_APPROX_SIMPLE
            )
            hull = np.squeeze(hull)
            cv2.polylines(
                frame,
                [hull],
                True,
                (0, 255, 0),
                2
            )
        msg = pickle.dumps(detections)
        sock.send(msg)

        cv2.imshow("img", frame)
        cv2.waitKey(1)
    cv2.destroyAllWindows()

if __name__ == '__main__':
    # 1. Load Config
    cfg_name = 'conf/network/jetson.yaml'
    with open(cfg_name, 'r') as file:
            cfg = yaml.safe_load(file)
    url = cfg['edge_url']
    port = cfg['edge_port']
    input = cfg['input']

    # 2. Establish Connection
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (url, port)
    print ('connecting to %s port %s' % server_address)
    sock.connect(server_address)

    # 3. Go
    run(input, start, bufferless=False)

    sock.close()