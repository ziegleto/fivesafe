
import cv2
import yaml
import pickle
from bev import draw_world_position
from decision_module import decision_making as dm
from utilities import connect_socket

def main(
    connection, 
    top_view_fname, 
) -> None:
    top_view_org = cv2.imread(top_view_fname) 

    while True:
        data = connection.recv(4096) #this can be an error, if size bigger. Truncated...
        tracks = pickle.loads(data)
        top_view = top_view_org.copy()

        # Draw Tracks
        for track in tracks:
            # Draw Positions in Top-View
            top_view = draw_world_position(top_view, track.world_position, track.id, track.color)
        cv2.imshow("top_view", top_view)
        cv2.waitKey(1)

if __name__ == '__main__':
    # 1. Load config 
    cfg_name = 'conf/network/client.yaml'
    with open(cfg_name, 'r') as file:
            cfg = yaml.safe_load(file)

    url = cfg['url']
    port = cfg['port']
    cfg_intended_paths = cfg['dm_intended_paths']
    cfg_turn_right = cfg['dm_turn_right']
    top_view_fname = cfg['top_view_fname']

    contours_int_path, color_int_path = dm.init_contours(cfg_intended_paths)
    contours_turn_right, color_turn_right = dm.init_contours(cfg_turn_right)

    # 2. Connect
    connection, _ = connect_socket(url, port)

    # 3. Go
    main(
        connection, 
        top_view_fname
    )