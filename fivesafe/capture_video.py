from utilities import MultiCameraCapture
import time
import cv2
import yaml


def main_loop():
    cfg_name = 'conf/inputs/camera_capture.yaml'
    with open(cfg_name, 'r') as file:
        cfg = yaml.safe_load(file)
    cameras = cfg['cameras']

    captured = MultiCameraCapture(cameras=cameras)
    start_time = time.time()
    dtav = 0
    record_flg = False

    while True:
        frames = captured.read_frames()
        if record_flg:
            for camera_name, frame in frames.items():
                captured.captures[camera_name].v_writer.write(frame)

        # Calculate FPS
        dt = time.time() - start_time
        start_time = time.time()
        dtav = .9*dtav+.1*dt #lowpass filter
        fps = 1/dtav

        k = captured.show_combined_frame(frames, fps, record_flg)
        
        if k == ord('q'):
            for camera_name, cap in captured.captures.items():
                cap.v_writer.release()
                cap.cap.release()
            cv2.destroyAllWindows()
            exit(1)
        elif k == 32: # space
            record_flg = not record_flg
            print(f'Record: {record_flg}')

if __name__ == '__main__':
    main_loop()