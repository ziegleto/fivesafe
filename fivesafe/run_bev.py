import cv2
from object_detection import find_detector_class
from image_tracking import Tracker, draw_track
from bev import PositionEstimation, draw_world_position
from utilities import run 
from decision_module import decision_making as dm
from ultralytics import YOLO
import torch

def start(cap, homography_fname, top_view_fname, model_name):
    device = torch.device("mps")
    # Intialize Detector, Position Estimator, CV2 Windows, and read Top View Image
    model = YOLO(model_name).to(device)
    DetectorClass = find_detector_class(model_name)
    detector = DetectorClass(model=model, classes_of_interest=['person', 'bicycle', 'car'])

    pos_est = PositionEstimation(homography_fname, 81/5) 
    cv2.namedWindow("top_view", cv2.WINDOW_NORMAL)
    cv2.namedWindow("perspective_view", cv2.WINDOW_NORMAL)
    top_view_org = cv2.imread(top_view_fname) 

    # Read Frame, Initialize Tracker
    ret, frame = cap.read()
    height, width, _ = frame.shape
    tracker = Tracker(width, height)
    tv_height, tv_width, _ = top_view_org.shape

    # Initialize Zones from Config
    CONTOURS_INT_PATH, COLOR_INT_PATH = dm.init_contours('conf/decision_making/not_intended_paths_contours.yaml') # TODO
    CONTOURS_TURNING_RIGHT, COLOR_TURN_RIGHT = dm.init_contours('conf/decision_making/turning_right_zone_contours.yaml') # TODO
    COLOR_STANDARD = (47, 255, 173)

    # Draw Zones into Top View 
    dm.draw_polylines_in_top_view(top_view_org, CONTOURS_INT_PATH, color=COLOR_INT_PATH)
    dm.draw_polylines_in_top_view(top_view_org, CONTOURS_TURNING_RIGHT, color=COLOR_TURN_RIGHT)

    # Loop over Timesteps
    while True:
        detections = detector.detect(frame)
        tracks = tracker.track(detections)
        top_view = top_view_org.copy()

        nr_of_objects = len(tracks)

        for track in tracks:
            # Transform Midpoint of Track to World Coordinates
            mask = detections[track.detection_id-1].mask
            world_position, _ = pos_est.map_entity_and_return_relevant_points(track, mask)
            #world_position = pos_est.transform_point(track.get_midpoint()) # Only use midpoint
            world_position = (world_position[0], world_position[1])

            # Color Code Situations
            color = COLOR_STANDARD
            if dm.is_pt_in_contours(world_position, CONTOURS_INT_PATH):
                color = COLOR_INT_PATH
            elif dm.is_pt_in_contours(world_position, CONTOURS_TURNING_RIGHT):
                color = COLOR_TURN_RIGHT
            if dm.is_crowded(nr_of_objects, 1):
                frame = dm.draw_crowded(frame, width, height)
                top_view = dm.draw_crowded(
                    top_view, 
                    tv_width, 
                    tv_height, 
                    thickness=40, 
                    font_thickness=5, 
                    font_scale=5, 
                    y_offset=200
                )

            # Draw in Perspective- and Top-View
            top_view = draw_world_position(top_view, world_position, track.id, color)
            frame = draw_track(frame, track, color=color, draw_detection_id=False)
        cv2.imshow("perspective_view", frame)
        cv2.imshow("top_view", top_view)
        cv2.waitKey(1)
        ret, frame = cap.read()
    cv2.destroyAllWindows()

if __name__ == '__main__':
    run('conf/inputs/video_vup.yaml', start, bufferless=False)